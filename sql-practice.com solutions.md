## SQL Practice

#### EASY

1. Show first name, last name, and gender of patients whose gender is 'M'

```sh
SELECT first_name, last_name, gender FROM patients WHERE gender = 'M';
```

2. Show first name and last name of patients who does not have allergies (null).

```sh
SELECT first_name, last_name FROM patients WHERE allergies IS NULL;
```

3. Show first name of patients that start with the letter 'C'

```sh
SELECT first_name FROM patients WHERE first_name LIKE 'C%'
```

4. Show first name and last name of patients that weight within the range of 100 to 120 (inclusive)

```sh
SELECT first_name, last_name FROM patients WHERE weight>=100 and weight<=120;
```

```sh
SELECT first_name, last_name FROM patients WHERE weight BETWEEN 100 and 120;
```

5. Update the patients table for the allergies column. If the patient's allergies is null then replace it with 'NKA'

```sh
UPDATE patients SET allergies = 'NKA' WHERE allergies IS NULL;
```

6. Show first name and last name concatinated into one column to show their full name.

```sh
SELECT CONCAT(first_name, ' ', last_name) AS full_name FROM patients;
```

7. Show first name, last name, and the full province name of each patient.
   Example: 'Ontario' instead of 'ON'

```sh
SELECT pat.first_name,pat.last_name, prov.province_name FROM patients as pat, provinces AS prov WHERE pat.province_id = prov.province_id;
```

```sh
SELECT first_name,last_name,province_name FROM patients
  JOIN provinces on provinces.province_id = patients.province_id;
```

#### MEDIUM

1. Show unique birth years from patients and order them by ascending. (birth date format - YYYY-MM-DD)

```sh
SELECT DISTINCT YEAR(birth_date) FROM patients ORDER BY birth_date ASC;
```

```sh
SELECT DISTINCT YEAR(birth_date) AS birth_year FROM patients ORDER BY birth_year;
```

2. Show unique first names from the patients table which only occurs once in the list.
   For example, if two or more people are named 'John' in the first_name column then don't include their name in the output list. If only 1 person is named 'Leo' then include them in the output.

```sh
SELECT first_name FROM patients group by 1 having count(first_name)=1
```

```sh
SELECT first_name FROM patients GROUP BY first_name HAVING COUNT(first_name) = 1
```

3. Show patient_id and first_name from patients where their first_name start and ends with 's' and is atleast 5 characters long.

```sh
SELECT patient_id, first_name FROM patients WHERE first_name like 's___%s'
```

4. Show patient_id, first_name, last_name from patients whos primary_diagnosis is 'Dementia'.
   Primary diagnosis is stored in the admissions table.

```sh
SELECT patients.patient_id, patients.first_name, patients.last_name 
FROM patients 
JOIN admissions 
WHERE patients.patient_id=admissions.patient_id
AND admissions.primary_diagnosis='Dementia'
```

```sh
SELECT patients.patient_id, first_name,last_name FROM patients
  JOIN admissions ON admissions.patient_id = patients.patient_id
  WHERE primary_diagnosis = 'Dementia';
```

5. Show patient_id, first_name, last_name from the patients table.
   Order the rows by the first_name ascending and then by the last_name descending.

```sh
SELECT patient_id, first_name, last_name FROM patients order by first_name ASC, last_name DESC;
```

6. Show the total amount of male patients and the total amount of female patients in the patients table

```sh
SELECT 
COUNT(CASE WHEN gender='M' THEN 1 ELSE null END) AS male_count,
COUNT(CASE WHEN gender='F' THEN 1 ELSE null END) AS female_count,
FROM patients;
```

```sh
SELECT (SELECT COUNT(*) FROM patients WHERE gender='M') AS male_count, 
  (SELECT COUNT(*) FROM patients WHERE gender='F') AS female_count;  
```

7. Show first and last name, allergies from patients which have allergies to either 'Penicillin' or 'Morphine'. Show results ordered ascending by allergies then by first_name then by last_name.

```sh
SELECT first_name,last_name, allergies FROM patients WHERE allergies ='Penicillin' 
OR allergies= 'Morphine' ORDER BY allergies,first_name,last_name;  
```

```sh
SELECT first_name, last_name, allergies FROM patients
  WHERE allergies IN ('Penicillin', 'Morphine')
  ORDER BY allergies, first_name, last_name;
```

8. Show patient_id, primary_diagnosis from admissions. Find patients admitted multiple times for the same primary_diagnosis.

```sh
SELECT patient_id, primary_diagnosis FROM admissions 
  GROUP BY patient_id, primary_diagnosis   
  HAVING COUNT(*)>1;
```

9. Show the city and the total number of patients in the city in order from most to least patients.

```sh
SELECT city, COUNT (patient_id) 
FROM patients 
GROUP BY city
ORDER BY COUNT (patient_id) DESC
```

#### HARD

1. Show all of the patients grouped into weight groups.
   Show the total amount of patients in each weight group.
   Order the list by the weight group decending.
   For example, if they weight 100 to 109 they are placed in the 100 weight group, 110-119 = 110 weight group, etc.

```sh
 SELECT COUNT(*) AS patients_in_group, weight/10*10 AS weight_group FROM patients
  GROUP BY weight_group
  ORDER BY weight_group desc;
```

2. Show patient_id, weight, height, isObese from the patients table.
   Display isObese as a boolean 0 or 1.
   Obese is defined as weight(kg)/(height(m)2) >= 30.
   weight is in units kg.
   height is in units cm.

```
SELECT patient_id, weight, height, 
  (CASE 
      WHEN weight/(POWER(height/100.0,2)) >= 30 THEN
          1
      ELSE
          0
      END) AS isObese
FROM patients;
```

3. Show patient_id, first_name, last_name, and attending physician's specialty.
   Show only the patients who has a primary_diagnosis as 'Dementia' and the physician's first name is 'Lisa'
   Check patients, admissions, and physicians tables for required information.

```sh
SELECT pat.patient_id, pat.first_name, pat.last_name, phy.specialty 
FROM admissions ad
JOIN patients AS pat ON pat.patient_id = ad.patient_id 
JOIN physicians AS phy ON phy.physician_id = ad.attending_physician_id 
WHERE ad.primary_diagnosis IS 'Dementia' AND phy.first_name IS 'Lisa'
```
